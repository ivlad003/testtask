package activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import model.ModelRow;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import requests.API;

import adapter.AdapterMainList;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.example.testtask.R;

@SuppressLint("NewApi")
public class MainActivity extends Activity {
	ListView list;
	List<Object> likeRowItem;
	AdapterMainList adapter;
	API api;
	AQuery aq;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		aq.ajax(api.geStringJson(), JSONObject.class, this, "getJson");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		list = (ListView) findViewById(R.id.list);
		api = new API();
		aq = new AQuery(this);

	}

	public void getJson(String url, JSONObject json, AjaxStatus status) {

		if (json != null) {
			try {
				likeRowItem = new ArrayList<Object>();

				Iterator itr = json.keys();

				int i = 0;
				while (itr.hasNext()) {
					String key = itr.next().toString();

					ModelRow item = new ModelRow(key, json.getString(key));
					likeRowItem.add(item);

					i++;
				}

				Collections.sort(likeRowItem, ModelRow.ModelRowKayComparator);

				adapter = new AdapterMainList(this, likeRowItem,
						R.layout.row_main_list);

				list.setAdapter(adapter);
				
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
		}
	}

}
