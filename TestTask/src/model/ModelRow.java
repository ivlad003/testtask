package model;

import java.util.Comparator;

public class ModelRow {
	String kay;
	String value;

	public ModelRow(String kay, String value) {
		super();
		this.kay = kay;
		this.value = value;
	}

	public String getKay() {
		return kay;
	}

	public void setKay(String kay) {
		this.kay = kay;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static Comparator<Object> ModelRowKayComparator = new Comparator<Object>() {

		public int compare(Object s1, Object s2) {
			
			String ModelRowKay1 = ((ModelRow)s1).getKay().toUpperCase();
			String ModelRowKay2 = ((ModelRow)s2).getKay().toUpperCase();

			// ascending order
			return ModelRowKay1.compareTo(ModelRowKay2);

			// descending order
			// return StudentName2.compareTo(StudentName1);
		}
	};
}
