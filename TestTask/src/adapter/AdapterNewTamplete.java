package adapter;
import java.util.List;

import model.ModelRow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class AdapterNewTamplete extends BaseAdapter implements View.OnClickListener{

	private List<Object> data;
	protected Context cntx;
	protected LayoutInflater mInflater;
	protected int mItemResId;

	public AdapterNewTamplete(Context context,
			List<Object> likeRowItem, int mItemResIdm) {
		init(context, likeRowItem, mItemResIdm);
	}

	private void init(Context context,
			List<Object> likeRowItem, int itemResId) {

		this.data = likeRowItem;
		this.mItemResId = itemResId;
		mInflater = LayoutInflater.from(context);
		cntx = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	abstract public View getView(int position, View convertView,
			ViewGroup parent);

	@Override
	abstract public void onClick(View v); 
	
	abstract class ViewHolder{};
}
