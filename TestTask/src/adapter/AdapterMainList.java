package adapter;

import java.util.List;

import model.ModelRow;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.testtask.R;

public class AdapterMainList extends AdapterNewTamplete {

	public AdapterMainList(Context context, List<Object> likeRowItem,
			int mItemResIdm) {
		super(context, likeRowItem, mItemResIdm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			convertView = this.mInflater
					.inflate(this.mItemResId, parent, false);
			holder = new ViewHolder();

			holder.textKay = (TextView) convertView.findViewById(R.id.textKay);

			holder.textValue = (TextView) convertView
					.findViewById(R.id.textValue);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		ModelRow item = (ModelRow) getItem(position);

		holder.textKay.setText(item.getKay());
		holder.textValue.setText(item.getValue());

		return convertView;
	}

	class ViewHolder {
		TextView textKay;
		TextView textValue;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
